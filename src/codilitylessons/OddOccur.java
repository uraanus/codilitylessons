/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package codilitylessons;

/**
 *
 * @author uranus
 * https://app.codility.com/programmers/lessons/2-arrays/odd_occurrences_in_array/
 */
public class OddOccur {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int [] A = {9, 3, 9, 3, 9, 7, 9} ;
       System.out.println(solution(A));
    }
    public static int solution(int[] A) {
        // write your code in Java SE 8
        
        int unpaired = 0;
		for (int number : A) {
			unpaired ^= number;
		}
		return unpaired;
	}
}
