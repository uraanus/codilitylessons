/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package codilitylessons;

/**
 *
 * @author uranus
 * https://app.codility.com/programmers/lessons/3-time_complexity/perm_missing_elem/
 */
public class PermMissingElement {
    public static void main(String args[]){
            int[] A={2,3};
        System.out.print(solution(A));

    }

      public static int solution(int[] A) {
        if(A.length==0)return 1;
        if(A.length==1) {
            if(A[0]==1)return 2;
            return 1;
        }
        long maxNumber=A[0];
        for(int i=0;i<A.length;i++){
            if(A[i]>maxNumber){
                maxNumber=A[i];
            }

        }
        if(maxNumber!=A.length+1) return A.length+1;
        long sumOfArrayOfN=(maxNumber*(maxNumber+1))/2;
        long sum=0;
        for(int i=0;i<A.length;i++){
            sum+=A[i];
        }
        return (int) (sumOfArrayOfN-sum);
    }
}
